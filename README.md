## Microbiome Overview
1. [Microbiome Overview](content/lecture_slides/microbiome_overview.pdf)

## Microbiome Bioinformatic Analysis
1. [Bioinformatic Analysis Overview](content/lecture_slides/dada2_pipeline.pdf)
2. [Demultiplex](content/bioinformatics/demultiplex.Rmd)
3. [DADA2: From FASTQs to OTUs](content/bioinformatics/dada2_tutorial_1_6.Rmd)

## Microbiome Statistical Analysis
2. [Absolute Abundance Plots](content/biostats/absolute_abundance_plots.Rmd)
3. [Alpha Diversity](content/biostats/alpha_diversity.Rmd)
4. [Relative Abundance](content/biostats/relative_abundance.Rmd)
5. [Beta Diversity & Ordination](content/biostats/ordination.Rmd)

## Appendix
1. [Configuration File](content/config.R)
2. [Download Atacama FASTQs](content/prep/atacama_download.Rmd)
3. [Download Taxonomic References](content/prep/download_dada_references.Rmd)

## Computing Environments
- [DCC OnDemand URL](https://dcc-ondemand-01.oit.duke.edu/)
- [Getting started with DCC OnDemand](misc/ondemand_howto.md)

## Workshop Content
- [Initial download of workshop content](misc/git_cloning.md)


**This website can be reached at: https://duke.is/premier2024**

![QR Code for this Gitlab Repo](misc/images/premier2024_qr.png){width=30% height=30%}
